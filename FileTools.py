import os
import scipy
from os import listdir
from os.path import isfile, join

def getFileList(baseDir, sortFileBy):
    onlyfiles = [f for f in listdir(baseDir) if isfile(join(baseDir, f))]

    if sortFileBy == "TIME":
        onlyfiles.sort(key=lambda x: os.path.getmtime(join(baseDir, x)))
    elif sortFileBy == "NAME":
        onlyfiles.sort(key=lambda x: x)

    return onlyfiles

def saveArrayAsImage(image_array, outfile):
    scipy.misc.imsave(outfile, image_array)

#Open a file and write all pixels coordinate of the circle contour.
def writeContourPixels(ptX, ptY, filename):
    text_file = open(filename, "w")

    for i in range(len(ptX)):
        text_file.write(str(ptX[i]) + '\t' + str(ptY[i]) + '\n')

    text_file.close()

import os
import errno


def createFileIfNotExist(fileToCreate):
    flags = os.O_CREAT | os.O_EXCL | os.O_WRONLY

    try:
        file_handle = os.open(fileToCreate, flags)
    except OSError as e:
        if e.errno == errno.EEXIST:  # Failed as the file already exists.
            return False
            pass
        else:  # Something unexpected went wrong so reraise the exception.
            raise
    else:  # No exception, so the file must have been created successfully.
        with os.fdopen(file_handle, 'w') as file_obj:
            # Using `os.fdopen` converts the handle to an object that acts like a
            # regular Python file object, and the `with` context manager means the
            # file will be automatically closed when we're done with it.
            file_obj.write("Look, I'm writing to a new file!")
            file_obj.close()
            return True
