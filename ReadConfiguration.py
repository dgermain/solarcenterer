

##This File read a TextFile, and extract pair of Name - Value
##To be used to set program execution parameters

def readConfigFile(filename):
    file = open(filename, mode='r')
    parameters = {}
    for line in file:
        line = line.strip()
        #Skip comments lines
        if line.startswith("#"):
            continue

        if "\t" in line:
            parts = line.split("\t")
            if len(parts) != 2:
                print("Line with wrong number of parameters:" + line)
                continue
            parameters[parts[0]] = parts[1]
    return parameters

def getFolderPath(key, params):
    folderPath = None
    if key in params.keys():
        folderPath = params[key]

    if len(folderPath) == 0:
        raise Exception("Missing folder:" + key)
    elif not folderPath.endswith("/"):
        folderPath = folderPath + "/"

    return folderPath


def getBoolValue(key, params, default):
    if key in params.keys():
        if params[key].lower() == "true":
            return True
        elif params[key].lower() == "false":
            return False
        else:
            raise ValueError("Unable to read correctly boolean for: " + key + " Value: "+ params[key])
    else:
        return default

def getIntValue(key, params, default):
    if key in params.keys():
        return int(params[key])

        #raise ValueError("Unable to read correctly boolean for: " + key + " Value: "+ params[key])
    else:
        return default


if __name__ == "__main__":
    parameters  = readConfigFile("./DefaultConfig.txt")

    for k,v in parameters.items():
        print("Read: " + k + " Value: " + v)