import numpy
from PIL import Image
import tifffile as tiff
import scipy
import scipy.misc

# This function open the original image.
# It then create a binarize verion of the image for processing.
# Returns 3 arrays:
# - Original Image array
# - Binarized image for center processing
# - Reduced and binarized image for faster center finding.
def getImageArray(filename, resizeFactor, thresholdValue):
    im = Image.open(filename)

    #Load the image in a numpy Array, allowing to use numpy math function on image
    full_image = numpy.array(im)
    #Tranform image in a binary form using the given threshold value as a cutoff for black or white
    work_image = binarize_array(full_image, thresholdValue)

    small_image = None

    if (resizeFactor == 1):
        small_image = work_image
    else:
        sizeX, sizeY = im.size
        smallerIm = im.resize((int(sizeX / resizeFactor), int(sizeY / resizeFactor)))
        small_image = numpy.array(smallerIm)
        small_image = binarize_array(small_image, thresholdValue)

    return full_image, work_image, small_image


# This function open the original image as the previous one. But for Color Tiff images
# A different library is needed for file opening..
# It then create a binarize verion of the image for processing.
# Returns 3 arrays:
# - Original Image array
# - Binarized image for center processing
# - Reduced and binarized image for faster center finding.
def getColorImageArray(filename, resizeFactor, thresholdValue):
    tif_image = tiff.imread(filename)

    #Only use the Blue Channel (R,G,B). Monochromatic image approximation.
    full_image = tif_image[:, :, 1]

    sizeX, sizeY = full_image.shape

    small_image = scipy.misc.imresize(full_image, (int(sizeX / resizeFactor), int(sizeY / resizeFactor)))
    #small_image = numpy.array(Image.fromarray(full_image).resize((int(sizeX / resizeFactor), int(sizeY / resizeFactor))))

    work_image = scipy.misc.imresize(full_image, (int(sizeX / 1), int(sizeY / 1)))
    work_image = binarize_array(work_image, thresholdValue)

    small_image = binarize_array(small_image, thresholdValue)

    return tif_image, work_image, small_image


def drawCenterCross(image_array, centerX, centerY, crossLength):
    halfLength = int(crossLength /2)
    for i in range(100):
        image_array[int(centerY) - halfLength + i][int(centerX)] = 0
        image_array[int(centerY)][int(centerX) - halfLength + i] = 0
        image_array[int(centerY) - halfLength + i][int(centerX + 1)] = 255
        image_array[int(centerY + 1)][int(centerX) - halfLength + i] = 255

#Set one pixel to a specific color...
def putpixel(image_array, pixelX, pixelY, color):
    #print(image_array[int(pixelY)][int(pixelX)])
    image_array[int(pixelY)][int(pixelX)] = color

#Center the image by rolling the image in Y and X, so that the specified center end up at the center of the image
def centerPoint(image_array, centerX, centerY):
    sizeX = len(image_array[0])
    sizeY = len(image_array)

    shiftX = int(centerX) - int(sizeX / 2)
    shiftY = int(centerY) - int(sizeY / 2)

    # Axe 0 is vertical, axis 1 is horizontal
    centeredImage = numpy.roll(image_array, -shiftY, axis=0)
    centeredImage = numpy.roll(centeredImage, -shiftX, axis=1)

    return centeredImage

## Return a numpy array with everything being 255 or 0, using a specified threshold
def binarize_array(numpy_array, threshold=200):
    """Binarize a numpy array."""
    numpy_array = numpy.where(numpy_array > threshold, 255, 0)
    return numpy_array


# Find center based on average coordinate of white pixels...
def findWeightCenter(image_array):
    sumX = 0.0
    sumY = 0.0
    countPixel = 0

    for i in range(len(image_array)):
        for j in range(len(image_array[0])):
            if image_array[i][j] == 255:
                sumX += i
                sumY += j
                countPixel += 1
            else:
                continue
    centerX = float(sumX) / float(countPixel)
    centerY = float(sumY) / float(countPixel)
    return (centerX, centerY)


# Find the minimum bounding box including all white pixels
def findBoundingBox(circle_array):
    minX = 100000
    minY = 100000
    maxX = 0.0
    maxY = 0.0

    for i in range(len(circle_array)):
        for j in range(len(circle_array[0])):
            if circle_array[i][j][0] == 255:
                minX = min(j, minX)
                minY = min(i, minY)
                maxX = max(j, maxX)
                maxY = max(i, maxY)
            else:
                continue

    sizeX = maxX - minX
    sizeY = maxY - minY

    return (sizeX, sizeY)

#Find first non-zero value in a one-dimensional array
def findFirstNonZero(array):
    for i in range(len(array)):
        if array[i] > 0:
            return i

    return -1

#Find last non-zero value in a one-dimensional array
def findLastNonZero(array):
    for i in range(len(array)):
        if array[len(array) - 1 - i] > 0:
            return len(array) - 1 - i

    return -1

#Extract all white pixel that have at least one dark pixel neighbor
def getContourPixels(sun_array):
    ptX = []
    ptY = []

    # Project the image on each XY axis by summing the pixel.
    # This allow to easily determine the limits of the sun
    sumAxeVertical = numpy.sum(sun_array, axis=1)
    sumAxeHorizontal = numpy.sum(sun_array, axis=0)

    startPosX = findFirstNonZero(sumAxeHorizontal)
    endPosX = findLastNonZero(sumAxeHorizontal)

    if startPosX < 1:
        startPosX = 1

    if endPosX < 0 or endPosX >= len(sumAxeHorizontal):
        endPosX = len(sumAxeHorizontal) - 1

    # imageShape = sun_array.shape

    ##Question: I removed the last pixel to avoid accessing outside of boundary when checking +1
    ##Should I remove the first pixel as well, just to be sure I do not access the -1 position of the array...

    for i in range(1, len(sumAxeVertical) - 1):
        if (sumAxeVertical[i] == 0):
            continue

        for j in range(startPosX, endPosX):
            if sun_array[i][j] == 255:
                if (sun_array[i - 1][j] == 0 or sun_array[i + 1][j] == 0 or sun_array[i][j - 1] == 0 or sun_array[i][
                    j + 1] == 0):
                    ptX.append(j)
                    ptY.append(i)
            else:
                continue

    return numpy.asarray(ptX) , numpy.asarray(ptY)

#Draw a circle in an array centered around a specific point.
def drawCircle(image_array, x0, y0, radius, color):
    x = radius - 1
    y = 0
    dx = 1
    dy = 1
    err = dx - (radius * 2)

    while (x >= y):
        putpixel(image_array, x0 + x, y0 + y, color)
        putpixel(image_array, x0 + y, y0 + x, color)
        putpixel(image_array, x0 - y, y0 + x, color)
        putpixel(image_array, x0 - x, y0 + y, color)
        putpixel(image_array, x0 - x, y0 - y, color)
        putpixel(image_array, x0 - y, y0 - x, color)
        putpixel(image_array, x0 + y, y0 - x, color)
        putpixel(image_array, x0 + x, y0 - y, color)

        if (err <= 0):
            y += 1
            err += dy
            dy += 2
        if (err > 0):
            x -= 1
            dx += 2
            err += dx - (radius * 2)
