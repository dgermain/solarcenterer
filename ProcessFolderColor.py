#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 14:50:17 2018
    
@author: davidgermain
"""

import numpy
import time
import math
import argparse
from ReadConfiguration import readConfigFile, getBoolValue, getFolderPath, getIntValue
import ImageTools
import FileTools
import tifffile as tiff
from PIL import Image
from ImageTools import *
import concurrent.futures

def findDist(x1,y1,x2,y2):
    return math.sqrt(((x1-x2) **2) + ((y1-y2)**2) )

def findPixScore(i,j,ptX,ptY,radius):
    ##Consider the distance of all contour pixels to the pt(i,j)
    distArray = numpy.sqrt(numpy.square(ptX-i) + numpy.square(ptY-j))

    ##Count the pixel that are at a radius distance of this center (squaring the diff <3)
    return numpy.sum(numpy.square(distArray - radius) < 1)

    ##Testing if the math is the same...
    # distArray = numpy.square(ptX-i) + numpy.square(ptY-j)
    #
    # # Count the pixel that are at a radius distance of this center (squaring the diff <3)
    # sqRadius = radius * radius
    # return numpy.sum(distArray - sqRadius < 9 )

def writeOffsetsToFile( outPutFile, centersOffset):
    #Open File

    #For list
    for item in centersOffset:
        print(item[0] + "\t" + str(item[1])+ "\t" + str(item[2]))

    #Save File
    print("File Saved: " + outPutFile)

def findCircleCenter(sun_array,radius,infoX,infoY,textFile):

    doWriteContourPixelToFile = len(textFile) > 0

    maxPixDist = -1
    bestCenter = (0, 0)

    guessX, guessY = 0,0
    x1,x2,y1,y2 = 0,0,0,0

    nptX, nptY = ImageTools.getContourPixels(sun_array)
    # print('Found Contour Pixels: ' + str(len(nptX)))

    if infoX == -1 or doWriteContourPixelToFile:
        if doWriteContourPixelToFile:
            FileTools.writeContourPixels(nptY,nptX,textFile)

        #Pick the center as average of the x and y positions
        guessX = int(numpy.sum(nptX)/len(nptX))
        guessY = int(numpy.sum(nptY)/len(nptY))

        #Create bounding box of place to check, using radius
        x1 = max(0,int(guessX-radius))
        x2 = min(len(sun_array),int(guessX+radius))
        y1 = max(0,int(guessY-radius))
        y2 = min(len(sun_array[0]),int(guessY+radius))

    #Overwrite Guess if actual coordinate were given
    if(infoX >= 0):
        x1 = infoX - 20
        x2 = infoX + 20
    if(infoY >= 0):
        y1 = infoY - 20
        y2 = infoY + 20

    scoreList = []
    #Need to find center of circle based on pixels...
    for i in range(x1,x2):
        for j in range(y1,y2):
            score = findPixScore(i,j,nptX,nptY,radius)
            scoreList.append((score, i, j))
            if maxPixDist == -1 or score > maxPixDist:
                maxPixDist = score
                bestCenter = (i,j)

    sorted_list = sorted(scoreList, key=lambda x: x[0])
    #scoreList.sort()

    if infoX != -1:
        for i in sorted_list[-6:]:
            print i

    return bestCenter

def processFolder(inputFolder, outputFolder, referenceImage, offsetFile, sortFileBy, drawCircleOverlay,useColorImages, outputFinaleImages, binaryThreshold, radiusOffset, maxFileCount,skipProcessedImage, saveContour, useFirstFileAsReference, debugMode):

    verbose = False

    filesToProcess = FileTools.getFileList(inputFolder, sortFileBy)
    filesAlreadyProcessed = []

    #Do not process image if the file already exist in target folder
    if skipProcessedImage:
        filesAlreadyProcessed = FileTools.getFileList(outputFolder, sortFileBy)

    #Default size of the bounding box containing the sun if no reference image is specified
    #This is used to calculate the actual radius of the sun in the image.
    sizeX, sizeY = 2717, 2720 #2280, 2282

    if useFirstFileAsReference == True:
        referenceImage = inputFolder + filesToProcess[0]

    if referenceImage:
        fullIm, workIm, smallIm = None,None,None

        ## To Check ????
        # if (useColorImages):
        #     fullIm, workIm, smallIm = ImageTools.getColorImageArray(referenceImage, 1)

        if (useColorImages):
            fullIm, workIm, smallIm = ImageTools.getImageArray(referenceImage, 1, binaryThreshold)
        else:
            fullIm, workIm, smallIm = ImageTools.getImageArray(referenceImage, 1, binaryThreshold)

        sizeX,sizeY = ImageTools.findBoundingBox(workIm)
        print('Found Sun box size of Reference: ' + referenceImage + ' :  ' + str(sizeX) + " , " + str(sizeY))
    else:
        print('No reference image was provided.Using the default sun bounding box size: ' + str(sizeX) + " , " + str(sizeY))
    
    #print("BoundingBox sizeX: " + str(sizeX) + " sizeY: " + str(sizeY))
    #radiusOld = ((sizeX+sizeY)/4)
    radius = (sizeY / 2) + radiusOffset
    print("Radius: " + str(radius) + " Offset: " + str(radiusOffset))

    fileCount = 0
    maxCount = len(filesToProcess)
    allOffsets = []

    # with concurrent.futures.ThreadPoolExecutor() as executor:
    #     futures = [executor.submit(processFrame, filename) for filename in filesToProcess]
    #
    #     # Wait for all tasks to complete
    #     concurrent.futures.wait(futures)

    #processFrame(filename)
    for f in filesToProcess:
        fileCount += 1
        if maxFileCount > 0 and fileCount > maxFileCount:
            print("FileCount: " + str(fileCount) + " MaxFileCount: " + str(maxFileCount))
            print("Max Number of files corrected")
            break

        #Skip file already processed!!!
        if f in filesAlreadyProcessed:
            continue

        if f.startswith("."):
            continue

        if outputFinaleImages and not FileTools.createFileIfNotExist(outputFolder+f):
            print("File Already exist: " + outputFolder + f)
            #continue

        totalStart = time.clock()
        start = time.clock()
        print(f)
        resizeFactor = 10
        
        if(useColorImages):
            full_image, image_array, small_array = ImageTools.getColorImageArray(inputFolder + f, resizeFactor, binaryThreshold)
            if debugMode:
                print("Processing getColorImageArray")
        else:
            full_image, image_array, small_array = ImageTools.getImageArray(inputFolder + f, resizeFactor, binaryThreshold)
            if debugMode:
                print("Processing getImageArray")

        if verbose:
            print( "Open Image: " + str(time.clock() - start) + " s")

        start = time.clock()
        smallCenterX, smallCenterY = findCircleCenter(small_array,radius/resizeFactor,-1,-1, '')

        if verbose:
            print("Find Smaller Center: " + str(time.clock() - start)  + " s")

        if debugMode:
            miniSizeX, miniSizeY = small_array.shape
            print("Found Small Center: " + str(smallCenterX) +"," + str(smallCenterY) + "in image size:" +  str(miniSizeX) +","+str(miniSizeY))
            #drawCenterCross(small_array, smallCenterX, smallCenterY, 20)
            #FileTools.saveArrayAsImage(small_array, outputFolder + "frameSmall_" + f)

            workSizeX, workSizeY = image_array.shape
            print("Work image size:" + str(
                workSizeX) + "," + str(workSizeY))

            if drawCircleOverlay:
                print("Drawing Circle")
                drawCircle(image_array, bestCenterX, bestCenterY, radius, 125)

            workFileName = outputFolder + "frameWork_" + f
            workFileName = workFileName.replace(".tif", ".jpg")
            #FileTools.saveArrayAsImage(image_array, workFileName)


        #centerY,centerX = findWeightCenter(image_array)
        #print(f + ' Found Center: ' + str(centerX) + "," + str(centerY))

        txtFile = ''
        if saveContour:
            txtFile = outputFolder + f.replace(".tif", ".txt")
        
        start = time.clock()
        bestCenterX, bestCenterY = findCircleCenter(image_array,radius,smallCenterX*resizeFactor,smallCenterY*resizeFactor,txtFile)

        allOffsets.append((f,bestCenterX, bestCenterY))
        if verbose:
            print("Guessed From Small : " + str(smallCenterX*resizeFactor) + "," + str(smallCenterY*resizeFactor))
            print("Real Center : " + str(bestCenterX) + "," + str(bestCenterY))

            print("Find Center: " + str(time.clock() - start) + "s")


        #drawCenterCross(full_image, bestCenterX, bestCenterY, 100)

        if drawCircleOverlay:
            drawCircle(full_image, bestCenterX, bestCenterY, radius, [42000,0,0])

            #Draw Contour
            nptX, nptY = ImageTools.getContourPixels(image_array)

            #image_array[int(pixelY)][int(pixelX)] = color
            for i in range(len(nptX)):
                x = int(nptX[i])
                y = int(nptY[i])
                full_image[y][x] = [0,0,49000]

        if outputFinaleImages:
            centeredImage = ImageTools.centerPoint(full_image, bestCenterX, bestCenterY)

            if debugMode:
                drawCircle(image_array, bestCenterX, bestCenterY, radius, 125)
                workFileName = outputFolder + "frameWork_" + f
                workFileName = workFileName.replace(".tif", ".jpg")
                FileTools.saveArrayAsImage(image_array, workFileName)
                # ncenterY,ncenterX = findWeightCenter(centeredImage)
                # print(f + ' Found New Center: ' + str(ncenterX) + "," + str(ncenterY))

            if(useColorImages):
                tiff.imsave(outputFolder+f,centeredImage)
            else:
                FileTools.saveArrayAsImage(centeredImage, outputFolder+f)
        else:
            if verbose:
                print("Skip Final output.  Center: " + str(bestCenterX) +"," + str(bestCenterY) )

        totalElapsed = time.clock() - totalStart
        print("Total Time for image: " + str(totalElapsed) + "s - Image: "+ str(fileCount) + "/" + str(maxCount) )

    #All images were processed
    if len(offsetFile) > 0:
        writeOffsetsToFile(outputFolder + offsetFile, allOffsets)



if __name__ == "__main__":

    # Read parameters for centering program
    parser = argparse.ArgumentParser(description='Centering eclipse images')

    parser.add_argument("-c", "--config", default='./DefaultConfig.txt', help='Path to configuration file')

    parser.add_argument("-s", '--skip', dest='skip', default=False, action = 'store_true', help='Skip already processed images')

    # parser.add_argument("-b", "--batchinput", default='./data/', help='Location of XML file to process')
    # parser.add_argument("-v", '--verbose', dest='verbose', action='store_true')

    args = parser.parse_args()
    colorImages = True
    skipAlreadyProcessedImage = False

    if args.config:
        print("Reading config from file: " + args.config)
        parameters = readConfigFile(args.config)
    else:
        parameters = readConfigFile("Reading config from Default file: " + "Test003-CenteringWithCircle.txt")


    colorImages = getBoolValue("COLOR_IMAGE", parameters, True)
    skipAlreadyProcessedImage = getBoolValue("SKIP_ALREADY_PROCESSED", parameters, True)
    saveContourPixel = getBoolValue("WRITE_CONTOUR_PIXEL_TO_FILE", parameters, False)
    outputFinalImages = getBoolValue("SAVE_OFFSET_TIFF", parameters, True)

    debugMode = getBoolValue("DEBUG_MODE", parameters, False)
    drawCircleOverlay = getBoolValue("DRAW_CIRCLE_OVERLAY", parameters, False)

    if drawCircleOverlay:
        print("Drawing Circle")
    binaryThresholdValue = getIntValue("PIXEL_VALUE_THRESHOLD", parameters, 200)
    maxFileNumber = getIntValue("MAX_NUMBER_OF_FILES", parameters, -1)
    radiusOffset = getIntValue("RADIUS_OFFSET", parameters, 0)

    inputDir = getFolderPath("INPUT_DIR",parameters)
    outputDir = getFolderPath("OUTPUT_DIR",parameters)

    offsetFile = ""
    if "SAVE_OFFSETS_TO_FILE" in parameters.keys():
        offsetFile = parameters["SAVE_OFFSETS_TO_FILE"]
        print("Save offsets to file: " + offsetFile)

    sortFileBy = "NAME"
    if "SORT_FILE_BY" in parameters.keys():
        sortFileBy = parameters["SORT_FILE_BY"]
    print("Files will be sorted by : " + sortFileBy)

    if args.skip:
        skipAlreadyProcessedImage = True


    referenceImage = None
    useFirstFileAsReference = False
    if "REFERENCE_IMAGE" in parameters.keys():
        referenceImage = parameters["REFERENCE_IMAGE"]
    elif getBoolValue("USE_FIRST_FRAME_AS_REFERENCE", parameters, True):
        useFirstFileAsReference = True

    #Launch the actual centering of images
    processFolder(inputDir, outputDir, referenceImage, offsetFile, sortFileBy, drawCircleOverlay,colorImages, outputFinalImages, binaryThresholdValue, radiusOffset, maxFileNumber, skipAlreadyProcessedImage, saveContourPixel, useFirstFileAsReference, debugMode)

